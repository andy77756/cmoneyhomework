﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Stock_Api.Models;
using Stock_Api.Models.Dto;

namespace Stock_Api.Services
{
    public interface IStockService
    {
        public Task<bool> GetByDateAsync(string startDt, string endDt);
        public Task<IQueryable<StockData>> GetDataAsync(string stockId, int recentDays);
        public Task<IncreasinMaxDaysDto> GetDividendYieldAsync(string stockId, string startDt, string endDt);
        public Task<IQueryable<StockData>> GetPERTopCountAsync(string date, int topCount);
    }
}
