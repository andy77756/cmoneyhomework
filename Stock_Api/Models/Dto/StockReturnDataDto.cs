﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stock_Api.Models.Dto
{
    public class StockReturnDataDto
    {
        public string stat { get; set; }
        public string date { get; set; }
        public string title { get; set; }
        public List<string> fields { get; set; }
        public List<List<object>> data { get; set; }
        public string selectType { get; set; }
        public List<string> notes { get; set; }
    }
}
