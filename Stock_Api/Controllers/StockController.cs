﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Stock_Api.Services;

namespace Stock_Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockController : ControllerBase
    {
        private readonly IStockService stockService;

        public StockController(IHttpClientFactory httpFactory, IStockService stockService)
        {
            this.stockService = stockService;

        }
        /// <summary>
        /// 取得日期區間內所有證交所提供的股票資訊
        /// </summary>
        /// <param name="startDt">起始日期 ex. 2021-01-01</param>
        /// <param name="endDt">結束日期 ex. 2021-01-01</param>
        /// <returns></returns>
        [HttpGet]
        [Route("StockData/GetByDate")]
        public async Task<bool> GetByDate(string startDt, string endDt)
        {
            return await stockService.GetByDateAsync(startDt, endDt);
        }
        /// <summary>
        /// 依照證券代號 搜尋最近n天的資料
        /// </summary>
        /// <param name="stockiId">證券代號</param>
        /// <param name="recentDays">近n日</param>
        /// <returns></returns>
        [HttpGet]
        [Route("StockData/GetData")]
        public async Task<Object> GetDataAsync(string stockiId, int recentDays)
        {
            return await stockService.GetDataAsync(stockiId, recentDays);
        }
        /// <summary>
        /// 指定日期範圍、證券代號 顯示這段時間內殖利率 為嚴格遞增的最長天數並顯示開始、結束日期
        /// </summary>
        /// <param name="stockId">證券代號</param>
        /// <param name="startDt">起始日期 ex. 2021-01-01</param>
        /// /// <param name="endDt">結束日期 ex. 2021-01-01</param>
        /// <returns></returns>
        [HttpGet]
        [Route("StockData/GetDividendYield")]
        public async Task<object> GetDividendYieldAsync(string stockId, string startDt, string endDt)
        {

            return await stockService.GetDividendYieldAsync(stockId, startDt, endDt);
        }
        /// <summary>
        /// 指定特定日期 顯示當天本益比前n名
        /// </summary>
        /// <param name="date">日期 ex. 2021-01-01</param>
        /// <param name="topCount">前n名</param>
        /// <returns></returns>
        [HttpGet]
        [Route("StockData/PERTopCountAtDate")]
        public async Task<Object> GetPERTopCountAsync(string date,int topCount)
        {

            return await stockService.GetPERTopCountAsync(date, topCount);
        }
    }
}
