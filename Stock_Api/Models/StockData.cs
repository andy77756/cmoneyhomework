﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Stock_Api.Models
{
    public partial class StockData
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(10)]
        public string StockId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public double? DividendYield { get; set; }
        public int? DevidendYear { get; set; }
        [Column("PER")]
        public double? Per { get; set; }
        [Column("PBR")]
        public double? Pbr { get; set; }
        [StringLength(10)]
        public string FinacialReportDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
    }
}