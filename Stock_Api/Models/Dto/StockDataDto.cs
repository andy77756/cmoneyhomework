﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stock_Api.Models.Dto
{
    public class StockDataDto
    {
        /// <summary>
        /// 股票代號
        /// </summary>
        /// <value>股票代號</value>
        public string StockId { get; set; }
        /// <summary>
        /// 股票名稱
        /// </summary>
        /// <value>股票名稱</value>
        public string StockName { get; set; }
        /// <summary>
        /// 殖利率
        /// </summary>
        /// <value>殖利率</value>
        public string Dividend_Yield { get; set; }
        /// <summary>
        /// 股利年度
        /// </summary>
        /// <value>股利年度</value>
        public int Devidend_Year { get; set; }
        /// <summary>
        /// 本益比
        /// </summary>
        /// <value></value>
        public string PER { get; set; }
        /// <summary>
        /// 本益淨值比
        /// </summary>
        /// <value>本益淨值比</value>
        public string PBR { get; set; }
        /// <summary>
        /// 財報年/月
        /// </summary>
        /// <value>財報年/月</value>
        public string FinacialReportDate { get; set; }
    }
}
