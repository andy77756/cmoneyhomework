﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Stock_Api.Models;
using Stock_Api.Models.Dto;

namespace Stock_Api.Services
{
    public class StockService : IStockService
    {
        private readonly IRepository<StockData> repo;
        private readonly IHttpService httpService;

        public StockService(IRepository<StockData> repo, IHttpService httpService)
        {
            this.repo = repo;
            this.httpService = httpService;

        }
        public async Task<bool> GetByDateAsync(string startDt, string endDt)
        {
            var sdt = Convert.ToDateTime(startDt);
            var edt = Convert.ToDateTime(endDt);
            var days = (sdt - edt).Days < 0 ? (edt - sdt).Days : (sdt - edt).Days;

            sdt = sdt < edt ? sdt : edt;
            for (int i = 0; i <= days; i++)
            {               
                var isExist = CheckDataExsit(sdt);
                if (!isExist)
                {
                    var sdtStr = sdt.ToString("yyyyMMdd");
                    var url = "https://www.twse.com.tw/exchangeReport/BWIBBU_d?response=json&date=" + sdtStr + "&selectType=ALL";
                    var responseMsg = await httpService.GetAsync(url);

                    if (!responseMsg.IsSuccessStatusCode)
                    {
                        return false;
                    }

                    var responseContent = await responseMsg.Content.ReadAsStringAsync();
                    var responseObject = JsonSerializer.Deserialize<StockReturnDataDto>(responseContent);

                    if (responseObject.data == null)
                    {
                        continue;
                    }
                    foreach (var datas in responseObject.data)
                    {
                        var temp = new StockData();
                        temp.Date = sdt;
                        for (int j = 0; j < datas.Count; j++)
                        {
                            switch (j)
                            {
                                case 0:
                                    temp.StockId = datas[j].ToString();
                                    break;
                                case 1:
                                    temp.Name = datas[j].ToString();
                                    break;
                                case 2:
                                    temp.DividendYield = datas[j].ToString() != null ? Convert.ToDouble(datas[j].ToString()) : null;
                                    break;
                                case 3:
                                    temp.DevidendYear = datas[j].ToString() != null ? Convert.ToInt32(datas[j].ToString()) : null;
                                    break;
                                case 4:
                                    double number;
                                    bool success = Double.TryParse(datas[j].ToString(), out number);
                                    if (success)
                                    {
                                        temp.Per = number;
                                    }
                                    break;
                                case 5:
                                    double number1;
                                    bool success1 = Double.TryParse(datas[j].ToString(), out number1);
                                    if (success1)
                                    {
                                        temp.Pbr = number1;
                                    }
                                    break;
                                case 6:
                                    temp.FinacialReportDate = datas[j].ToString() != null ? datas[j].ToString() : null;
                                    break;
                                default:
                                    break;
                            }
                        }

                        await repo.CreateAsync(temp);

                    }
                }
                sdt = sdt.AddDays(1);
            }
            await repo.SaveChangesAsync();

            return true;
        }

        public async Task<IQueryable<StockData>> GetDataAsync(string stockId, int recentDays)
        {
            var afterDate = DateTime.Now.AddDays(-recentDays).Date;
            var result = repo.ReadByCondition(c => c.StockId == stockId && c.Date > afterDate);

            return result;
        }

        public async Task<IQueryable<StockData>> GetPERTopCountAsync(string date, int topCount)
        {
            var dt = Convert.ToDateTime(date);
            var result = repo.ReadByConditionAndTakeTopCount(c => c.Date == dt, c => c.Per, topCount);

            return result;
        }

        public async Task<IncreasinMaxDaysDto> GetDividendYieldAsync(string stockId, string startDt, string endDt)
        {
            var sdt = Convert.ToDateTime(startDt);
            var edt = Convert.ToDateTime(endDt);
            var days = (sdt - edt).Days < 0 ? (edt - sdt).Days : (sdt - edt).Days;

            var temp = sdt;
            sdt = sdt < edt ? sdt : edt;
            edt = temp < edt ? edt : temp;

            var datas = repo.ReadByCondition(c => c.Date >= sdt && c.Date <= edt && c.StockId == stockId).OrderBy(c => c.Date).ToList();
             
            var result = new List<List<StockData>>();
            int index = 0;
            for (int i = 0; i < datas.Count(); i++)
            {
                if (i == 0 || datas.ElementAt(i).DividendYield < datas.ElementAt(i-1).DividendYield)
                {
                    if (i != 0)
                    {
                        index++;
                    }
                    result.Add(new List<StockData>());
                    result[index].Add(datas.ElementAt(i));
                }
                else result[index].Add(datas.ElementAt(i));
            }

            var returnData = new IncreasinMaxDaysDto();
            returnData.days = result.Max(c => c.Count);
            var a = result.FindAll(c => c.Count == returnData.days).Last();
            returnData.startDt = a.First().Date;
            returnData.endDt = a.Last().Date;


            return returnData;
        }

        private bool CheckDataExsit(DateTime sdt)
        {
            return repo.CheckExist(c => c.Date.Date == sdt.Date);
        }
    }


}
