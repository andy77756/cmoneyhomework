﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Stock_Api.Models
{
    public interface IRepository<T>
    {
        /// <summary>
        /// 確認符合條件的資料是否存在
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        bool CheckExist(Expression<Func<T, bool>> predicate);
        /// <summary>
        /// 新增一筆資料。
        /// </summary>
        /// <param name="entity">要新增到的Entity</param>
        Task CreateAsync(T entity);

        /// <summary>
        /// 取得第一筆符合條件的內容。如果符合條件有多筆，也只取得第一筆。
        /// </summary>
        /// <param name="predicate">要取得的Where條件。</param>
        /// <returns>取得第一筆符合條件的內容。</returns>
        T Read(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// 取得Entity全部筆數的IQueryable。
        /// </summary>
        /// <returns>Entity全部筆數的IQueryable。</returns>
        IQueryable<T> Reads();

        /// <summary>
        ///  取得符合條件全部筆數的IQueryable。
        /// </summary>
        /// <returns>Entity全部筆數的IQueryable。</returns>
        IQueryable<T> ReadByCondition(Expression<Func<T, bool>> predicate);
        /// <summary>
        ///  取得符合條件升冪排序後前n筆數的IQueryable。
        /// </summary>
        /// <returns>Entity全部筆數的IQueryable。</returns>
        IQueryable<T> ReadByConditionAndTakeLastCount(Expression<Func<T, bool>> predicate, Expression<Func<T, double?>> keySelector, int topCount);
        /// <summary>
        ///  取得符合條件降冪排序後前n筆數的IQueryable。
        /// </summary>
        /// <returns>Entity全部筆數的IQueryable。</returns>
        IQueryable<T> ReadByConditionAndTakeTopCount(Expression<Func<T, bool>> predicate, Expression<Func<T, double?>> keySelector, int topCount);

        /// <summary>
        /// 更新一筆資料的內容。
        /// </summary>
        /// <param name="entity">要更新的內容</param>
        void Update(T entity);

        /// <summary>
        /// 刪除一筆資料內容。
        /// </summary>
        /// <param name="entity">要被刪除的Entity。</param>
        void Delete(T entity);

        /// <summary>
        /// 儲存異動。
        /// </summary>
        Task SaveChangesAsync();
    }
}
