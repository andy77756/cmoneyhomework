﻿CREATE TABLE [dbo].[table2] (
    [studentid] INT        NOT NULL,
    [name]      NCHAR (10) NOT NULL,
    [chinese]   INT        NOT NULL,
    [english]   INT        NOT NULL,
    [math]      INT        NOT NULL,
    CONSTRAINT [PK_table2] PRIMARY KEY CLUSTERED ([studentid] ASC)
);

