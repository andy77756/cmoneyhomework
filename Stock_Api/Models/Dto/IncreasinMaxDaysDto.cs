﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stock_Api.Models.Dto
{
    public class IncreasinMaxDaysDto
    {
        public int days { get; set; }
        public DateTime startDt { get; set; }
        public DateTime endDt { get; set; }
    }
}
