﻿CREATE TABLE [dbo].[StockData] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [StockId]            NVARCHAR (10) NOT NULL,
    [Name]               NVARCHAR (50) NOT NULL,
    [DividendYield]      FLOAT (53)    NULL,
    [DevidendYear]       INT           NULL,
    [PER]                FLOAT (53)    NULL,
    [PBR]                FLOAT (53)    NULL,
    [FinacialReportDate] NVARCHAR (10) NULL,
    [Date]               DATETIME      NOT NULL,
    CONSTRAINT [PK_StockData_1] PRIMARY KEY CLUSTERED ([Id] ASC)
);

