﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Stock_Api.Models
{
    public class EFGenericRepository<TEntity> : IRepository<TEntity>
    where TEntity : class
    {
        private readonly StockContext inContext;


        /// <summary>
        /// 建構EF一個Entity的Repository，需傳入此Entity的Context。
        /// </summary>
        /// <param name="inContext">Entity所在的Context</param>
        public EFGenericRepository(StockContext inContext)
        {
            this.inContext = inContext;
        }

        public bool CheckExist(Expression<Func<TEntity, bool>> predicate)
        {
            var data = inContext.Set<TEntity>().Where(predicate).FirstOrDefault();
            if (data is null)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 新增一筆資料到資料庫。
        /// </summary>
        /// <param name="entity">要新增到資料的庫的Entity</param>
        public async Task CreateAsync(TEntity entity)
        {
            inContext.Set<TEntity>().AddAsync(entity);
        }

        /// <summary>
        /// 取得第一筆符合條件的內容。如果符合條件有多筆，也只取得第一筆。
        /// </summary>
        /// <param name="predicate">要取得的Where條件。</param>
        /// <returns>取得第一筆符合條件的內容。</returns>
        public TEntity Read(Expression<Func<TEntity, bool>> predicate)
        {
            return inContext.Set<TEntity>().Where(predicate).FirstOrDefault();
        }

        /// <summary>
        /// 取得符合條件全部筆數的IQueryable。
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IQueryable<TEntity> ReadByCondition(Expression<Func<TEntity, bool>> predicate)
        {
            return inContext.Set<TEntity>().Where(predicate);
        }
        /// <summary>
        /// 取得符合條件且升冪排序後前n筆的IQueryable
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="keySelector"></param>
        /// <param name="topCount"></param>
        /// <returns></returns>
        public IQueryable<TEntity> ReadByConditionAndTakeLastCount(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, double?>> keySelector, int topCount)
        {
            return inContext.Set<TEntity>().Where(predicate).OrderBy(keySelector).Take(topCount);
        }
        /// <summary>
        /// 取得符合條件且降冪排序後前n筆的IQueryable
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="keySelector"></param>
        /// <param name="topCount"></param>
        /// <returns></returns>
        public IQueryable<TEntity> ReadByConditionAndTakeTopCount(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, double?>> keySelector, int topCount)
        {
            return inContext.Set<TEntity>().Where(predicate).OrderByDescending(keySelector).Take(topCount);
        }


        /// <summary>
        /// 取得Entity全部筆數的IQueryable。
        /// </summary>
        /// <returns>Entity全部筆數的IQueryable。</returns>
        public IQueryable<TEntity> Reads()
        {
            return inContext.Set<TEntity>().AsQueryable();
        }

        /// <summary>
        /// 更新一筆Entity內容。
        /// </summary>
        /// <param name="entity">要更新的內容</param>
        public void Update(TEntity entity)
        {
            inContext.Entry<TEntity>(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// 更新一筆Entity的內容。只更新有指定的Property。
        /// </summary>
        /// <param name="entity">要更新的內容。</param>
        /// <param name="updateProperties">需要更新的欄位。</param>
        public void Update(TEntity entity, Expression<Func<TEntity, object>>[] updateProperties)
        {

            inContext.Entry<TEntity>(entity).State = EntityState.Unchanged;

            if (updateProperties != null)
            {
                foreach (var property in updateProperties)
                {
                    inContext.Entry<TEntity>(entity).Property(property).IsModified = true;
                }
            }
        }

        /// <summary>
        /// 刪除一筆資料內容。
        /// </summary>
        /// <param name="entity">要被刪除的Entity。</param>
        public void Delete(TEntity entity)
        {
            inContext.Entry<TEntity>(entity).State = EntityState.Deleted;
        }

        /// <summary>
        /// 儲存異動。
        /// </summary>
        public async Task SaveChangesAsync()
        {
            inContext.SaveChangesAsync();
        }


    }
}
