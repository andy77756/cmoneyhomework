﻿using Moq;
using NUnit.Framework;
using Stock_Api.Models;
using Stock_Api.Models.Dto;
using Stock_Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Stock_Api.Services.Tests
{
    [TestFixture()]
    public class StockServiceTests
    {
        [Test()]
        public async Task GetByDateAsyncTest()
        {
            //Arrange
            var mockHttpService = new Mock<IHttpService>();
            mockHttpService
                .Setup(httpService => httpService.GetAsync(It.IsAny<string>()))
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent("{\"stat\":\"OK\",\"date\":\"20210303\",\"title\":\"110年03月03日 個股日本益比、殖利率及股價淨值比\",\"fields\":[\"證券代號\",\"證券名稱\",\"殖利率(%)\",\"股利年度\",\"本益比\",\"股價淨值比\",\"財報年/季\"],\"data\":[[\"1101\",\"台泥\",\"7.05\",108,\"10.13\",\"1.30\",\"109/3\"]]}"),
                })
                .Verifiable();

            var mockRepo = new Mock<IRepository<StockData>>();
   


            var stockService = new StockService(mockRepo.Object, mockHttpService.Object);

            string sdt = "2021-04-01";
            string edt = "2021-04-01";
            

            var expect = true;

            //Act
            var act = await stockService.GetByDateAsync(sdt, edt);

            //Assert
            Assert.AreEqual(act, expect);
        }

        [Test()]
        public async Task GetDataAsyncTest()
        {
            //Arrange
            var mockHttpService = new Mock<IHttpService>();
            mockHttpService
                .Setup(httpService => httpService.GetAsync(""))
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent("{\"stat\":\"OK\",\"date\":\"20210401\",\"title\":\"110年04月01日 個股日本益比、殖利率及股價淨值比\",\"fields\":[\"證券代號\",\"證券名稱\",\"殖利率(%)\",\"股利年度\",\"本益比\",\"股價淨值比\",\"財報年 / 季\"],\"data\":[[\"1101\",\"台泥\",\"7.49\",109,\"11.04\",\"1.36\",\"109 / 4]]}"),
                })
                .Verifiable();

            string stockId = "2401";
            var afterDate = DateTime.Now.AddDays(-1).Date;
            var mockRepo = new Mock<IRepository<StockData>>();
            var datas = new List<StockData>()
            {
                new StockData
                {
                    Id = 1,
                    StockId = "1101",
                    Name = "台泥",
                    DividendYield = 7.47,
                    DevidendYear = 109,
                    Per = 1.36,
                    Pbr = null,
                    FinacialReportDate = "109/4",
                    Date = Convert.ToDateTime("2021-04-06 00:00:00.000")
                }               
            }.AsQueryable();
            mockRepo.Setup(mockRepo => mockRepo.ReadByCondition(c => c.StockId == stockId && c.Date > afterDate)).Returns(datas);

            var stockService = new StockService(mockRepo.Object, mockHttpService.Object);

            
            int recentDays = 1;

            var expect = datas; 

            //Act
            var act = await stockService.GetDataAsync(stockId, recentDays);

            //Assert
            Assert.AreEqual(act, expect);
        }

        [Test()]
        public async Task GetPERTopCountAsyncTest()
        {
            //Arrange
            var mockHttpService = new Mock<IHttpService>();

            string date = "2021-04-06";
            var dt = Convert.ToDateTime(date);
            int topCount = 5;
            var mockRepo = new Mock<IRepository<StockData>>();
            var datas = new List<StockData>() {
                new StockData {
                    Id = 1,
                    StockId = "1432",
                    Name = "大魯閣",
                    DividendYield = 26.4,
                    DevidendYear = 108,
                    Per = null,
                    Pbr = null,
                    FinacialReportDate = "",
                    Date = dt
                },
                new StockData {
                    Id = 2,
                    StockId = "2929",
                    Name = "淘帝-KY",
                    DividendYield = 21.76,
                    DevidendYear = 108,
                    Per = 0.18,
                    Pbr = null,
                    FinacialReportDate = "109/3",
                    Date = dt
                },
                new StockData {
                    Id = 3,
                    StockId = "1439",
                    Name = "中和",
                    DividendYield = 19.88,
                    DevidendYear = 108,
                    Per = 1.04,
                    Pbr = null,
                    FinacialReportDate = "109/4",
                    Date = dt
                },
                new StockData {
                    Id = 4,
                    StockId = "1325",
                    Name = "恆大",
                    DividendYield = 13.1,
                    DevidendYear = 109,
                    Per = 2.1,
                    Pbr = null,
                    FinacialReportDate = "109/4",
                    Date = dt
                },
                new StockData {
                    Id = 5,
                    StockId = "2601",
                    Name = "益航",
                    DividendYield = 12.26,
                    DevidendYear = 108,
                    Per = 0.85,
                    Pbr = null,
                    FinacialReportDate = "109/4",
                    Date = dt
                },
            }.AsQueryable();

           
            mockRepo.Setup(mockRepo => mockRepo.ReadByConditionAndTakeTopCount(It.IsAny<Expression<Func<StockData, bool>>>() , It.IsAny<Expression<Func<StockData, double?>>>(), It.IsAny<int>())).Returns(datas);
            var stockService = new StockService(mockRepo.Object, mockHttpService.Object);

            var expect = datas;
            //Act
            var act = await stockService.GetPERTopCountAsync(date, topCount);

            //Assert
            Assert.AreEqual(act, expect);
        }

        [Test()]
        public async Task GetDividendYieldAsyncTest()
        {
            //Arrange
            var mockHttpService = new Mock<IHttpService>();
            var mockRepo = new Mock<IRepository<StockData>>();
            var datas = new List<StockData>() {
                new StockData {
                    Id = 3790,
                    StockId = "1101",
                    Name = "台泥",
                    DividendYield = 7.49,
                    DevidendYear = 109,
                    Per = 11.04,
                    Pbr = 1.36,
                    FinacialReportDate = "109/4",
                    Date = Convert.ToDateTime("2021-04-09 00:00:00.000")
                },
                new StockData {
                    Id = 4736,
                    StockId = "1101",
                    Name = "台泥",
                    DividendYield = 7.47,
                    DevidendYear = 109,
                    Per = 11.08,
                    Pbr = 1.36,
                    FinacialReportDate = "109/4",
                    Date = Convert.ToDateTime("2021-04-12 00:00:00.000")
                },
                new StockData {
                    Id = 956,
                    StockId = "1101",
                    Name = "台泥",
                    DividendYield = 7.42,
                    DevidendYear = 109,
                    Per = 11.16,
                    Pbr = 1.37,
                    FinacialReportDate = "109/4",
                    Date = Convert.ToDateTime("2021-04-13 00:00:00.000")
                },
                new StockData {
                    Id = 6631,
                    StockId = "1101",
                    Name = "台泥",
                    DividendYield = 7.45,
                    DevidendYear = 109,
                    Per = 11.11,
                    Pbr = 1.37,
                    FinacialReportDate = "109/4",
                    Date = Convert.ToDateTime("2021-04-14 00:00:00.000")
                },
                new StockData {
                    Id = 5683,
                    StockId = "1101",
                    Name = "台泥",
                    DividendYield = 7.31,
                    DevidendYear = 109,
                    Per = 11.32,
                    Pbr = 1.39,
                    FinacialReportDate = "109/4",
                    Date = Convert.ToDateTime("2021-04-15 00:00:00.000")
                },
            }.AsQueryable();
            var stockService = new StockService(mockRepo.Object, mockHttpService.Object);

            mockRepo.Setup(mockRepo => mockRepo.ReadByCondition(It.IsAny<Expression<Func<StockData, bool>>>())).Returns(datas);

            string stockId = "1101";
            string sdt = "2021-04-09";
            string edt = "2021-04-15";


            var expect = new IncreasinMaxDaysDto()
            {
                days = 2,
                startDt = Convert.ToDateTime("2021-04-13 00:00:00.000"),
                endDt = Convert.ToDateTime("2021-04-14 00:00:00.000")
            };

            //Act
            var act = await stockService.GetDividendYieldAsync(stockId, sdt, edt);

            //Assert
            Assert.AreEqual(act, expect);
        }
    }
}