﻿CREATE TABLE [dbo].[product] (
    [startDate]   DATETIME      NOT NULL,
    [endDate]     DATETIME      NOT NULL,
    [price]       INT           NOT NULL,
    [productid]   INT           NULL,
    [productname] NVARCHAR (10) NULL
);

