﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Stock_Api.Services
{
    public class HttpService : IHttpService
    {
        private readonly IHttpClientFactory httpClientFactory;


        public HttpService(IHttpClientFactory httpClientFactory)
        {
            this.httpClientFactory = httpClientFactory;
        }
        public Task<HttpResponseMessage> GetAsync(string url)
        {
            var httpclient = httpClientFactory.CreateClient();
            return httpclient.GetAsync(url);
        }

    }
}
