# how to start
1. 需要 MSSQL server 2016 以上版本
2. 建立資料庫
3. 開啟DataBase專案下的DatabaseToProject.scmp(結構描述檔)
4. 左邊選取本專案(DataBase)，右邊請新增方才建立的資料庫連線並選取資料庫
5. 比較差異後更新
6. 開啟Stock專案下的appsettings.Development.json，修改connection連線字串
7. 完成後即可正常啟動stock專案

# API 規格 (啟動專案會直接開啟swagger首頁，url: https://localhost:44375/swagger/index.html)
1. Get /api/Stock/StockData/GetByDate           取得日期區間內所有證交所提供的股票資訊並寫入資料庫
2. Get /api/Stock/StockData/GetData             依照證券代號 搜尋最近n天的資料
3. Get /api/Stock/StockData/GetDividendYield    指定日期範圍、證券代號 顯示這段時間內殖利率 為嚴格遞增的最長天數並顯示開始、結束日期
4. Get /api/Stock/StockData/PERTopCountAtDate   指定特定日期 顯示當天本益比前n名

# 設計概念
1. 先依需求定義3個route
2. 開介面方法並宣告class實作此介面方法，並利用DI注入到Controller使用方法
3. 透過api批次取得股票資料，將資料儲存至資料庫
4. 為避免資料來源變更，因此定義IRepository介面方法，而目前我是使用Entityframework存取資料庫，所以寫一個EFGenericRepository實作IRepository方法，未來若有其他資料來源就只需抽換實作的class

