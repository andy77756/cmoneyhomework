﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Stock_Api.Services
{
    public interface IHttpService
    {
        Task<HttpResponseMessage> GetAsync(string url);
    }
}
